---
version: 2
titre: Analyse fine du trafic routier et de ses corrélations avec la qualité de l’air et le bruit

type de projet: Projet de semestre 6
année scolaire: 2021/2022
mandants:
  - projet FRI IoTnet
  - OFROU
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
mots-clé: [IoT, Smart city, Data Science, Analyse des corrélations, Trafic routier, Particules fines, Nuisances sonores]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/trafic.jpg}
\end{center}
```

## Contexte

Le projet FRI-IoTnet regroupant les villes de Fribourg et Bulle, les entreprises softcom, Franic SA, GESA, le groupe-e et Wifx a mis en place un système de mesure du trafic routier, de la qualité de l’air et du bruit. Le système est fonctionnel depuis plus d’une année. Les défauts de jeunesse sont pour ainsi dire éliminés mais il reste des travaux d’optimisation. 
La première partie du projet correspond à l’optimisation des mesures de trafic routier. Une possibilité intéressante est offerte par l’OFROU (Office Fédéral des ROUtes) qui propose d’échanger leurs données avec les données de FRI-IoTnet en utilisant un format « type ». De cette manière nous aurions une vision du trafic sur les axes ville ainsi que sur l’autoroute.
La deuxième partie est de rassembler les mesures de trafic et de les corréler avec les mesures de bruit et de pollution. Nous disposons de 4 caméras d’analyse de trafic et l’OFROU peut mettre à disposition 4 autres points de mesure. Les mesures de bruit ont été réalisées sur quinze locations et la pollution est mesurée en permanence sur les lignes de bus 1, 2, 5 et 6 de l’agglomération fribourgeoise. Il y a donc beaucoup de données à disposition. Ce travail de semestre se focalisera sur la sélection des mesures les plus intéressantes ainsi que sur la mise en relation des effets du trafic sur les pollutions atmosphériques et sonores.

## Objectifs

1)	Analyser et décrire les systèmes de mesures mis en place par le projet FRI-IoTnet
2)	Détecter les éventuels défauts de mesure et les corriger si possible
3)	Utiliser les données de l’OFROU pour avoir une vision générale du trafic dans toute l’agglomération fribourgeoise
4)	Faire une analyse de corrélation entre trafic routier, pollution et nuisances sonores sur des points clés préalablement choisis


## Contraintes

Utiliser les données smartcity de la plateforme « FRI-IoTnet » pour les analyses de corrélation entre le trafic, le bruit et la qualité de l’air. Utiliser le format d’échange de données de l’OFROU pour accéder à leurs mesures de trafic (auto)routier. 
