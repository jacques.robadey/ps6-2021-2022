---
version: 2
titre: une maison intelligente saine et économe en énergie

type de projet: Projet de semestre 6
année scolaire: 2021/2022
mandants:
  - entreprise WAGO
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Jean-Roland Schuler
mots-clé: [Bluetooth, LoRa, WLAN, MBus, IoT, domotique, système de contrôle et visualisation, stratégie énergétique 2050, environnement sain]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth]{img/smart_energy_home.png}
\end{center}
```

## Contexte

La stratégie énergétique 2050 est très ambitieuse et comme la moitié de la consommation se trouve dans les bâtiments, le contrôle de cet élément est crucial. De nombreuses solutions d’optimisation ont déjà été développées mais il reste un élément clé à définir : le système de communication entre la gestion centrale du bâtiment et ses équipements. A titre d’exemple l’entreprise WAGO prévoie déployer des systèmes de stockage thermique à BlueFactory, mais la gestion en fonction des températures externe, interne et du taux de la charge des batteries thermiques reste à développer.

Ce projet de semestre aura pour premier but de faire un état de l’art de la domotique et des technologies de communication dans les bâtiments. Il faudra pour ceci aussi bien étudier les technologies câblées comme Ethernet, MBus, KNX que sans fils comme Bluetooth, LoRa, wireless KNX et WLAN. Les deux technologies les plus prometteuses seront choisies et un mini banc de test avec des interrupteurs, des capteurs de température et de qualité de l’air seront construits. Une comparaison en réel entre les 2 technologies pourra ensuite être réalisée.
Notez que les interrupteurs et capteurs mentionnés seront utilisés par l’entreprise WAGO pour la gestion thermique de nouveaux locaux à BlueFactory. Son choix de la technologie de communication se basera donc sur les résultats de ce travail de semestre.
 

## Objectifs

1)	Faire l’état de l’art de l’IoT dans les bâtiments
2)	Réaliser 2 bancs de test avec deux technologies concurrentes et les analyser de manière critique
3)	Proposer une des deux technologies pour l’implanter dans le système de stockage thermique destiné à BlueFactory


## Contraintes

Utiliser des capteurs du marché avec une technologie de communication, suivre les consignes de l’entreprise WAGO pour pouvoir réutiliser les résultats du banc de test de manière concrète.
