---
version: 2
titre: Le Groupe E se lance dans l'IoT. Business Case gagnant ?

type de projet: Projet de semestre 6
année scolaire: 2021/2022
mandants:
  - groupe E
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Serge Ayer
mots-clé: [LoRa, IoT, business case, innovation, smartmeters, smart home, smart city]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/gestinergie.png}
\end{center}
```

## Contexte

Avec Gestinergie 4.0, le Groupe-e a réussi à gérer une multitude de services pour la commune de Chevroux : https://www.groupe-e.ch/fr/construire-renover/installations-infrastructures/professionnels/gestinergie. Ces services peuvent être vendus dans d’autres communes, mais ils fonctionnent soit en 3-, 4- ou 5G ce qui signifie que le groupe E reste dépendant des opérateurs télécom. Ce problème disparaitrait si le groupe E construisait son propre réseau IoT comme les Services Industriels de Genève (SIG) l’ont fait. Mais le jeu en vaut-il la chandelle dans le canton de Fribourg ? 
Le business case proposé par Florian Butcher « Responsable Innovation et Veille technologique du groupe E » doit pouvoir y répondre.
 

## Objectifs

1)	Faire une analyse techno-économique des réseaux et service IoT : 
    a.	Coûts des réseaux NBIoT, LoRa et SigFox (input possibles de swisscom/sunrise, SIG et FRI-IoTnet)
    b.	Établir une liste des services potentiels : contrôle de qualité de l’air, molok, gestion des déchets, contrôle des réseaux d’eau, risques d’inondation, îlots de chaleur, trafic routiers, maisons intelligentes et services internes du groupe E
    c.	Coûts de développement de nouveaux services (les coûts du développement de gestinergie4.0 pourraient être utilisés)
    d.	Les recettes envisagées des services IoT
2)	Proposer un déploiement du réseau et des services sur le canton de Fribourg sur quelques années et calculer les coûts, recettes potentielles durant ces années
3)	Faire ces calculs pour 2 stratégies : prudente (déploiement dans les centres urbains) et agressive (déploiement rapide du canton)


## Contraintes

Se baser sur les inputs du groupe E connect. Les connaissances gagnées par l’école grâce au projet FRI-IoTnet doivent aussi être utilisées.
