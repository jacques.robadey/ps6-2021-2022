---
version: 2
titre: Un réseau qui crée de la bande passante à la demande

type de projet: Projet de semestre 6
année scolaire: 2021/2022
mandants:
  - TP de conception de réseau
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Patrick Gaillet
mots-clé: [management de réseau, configuration de réseau, bande passante à la demande, protection, lambda switching, analyse techno-économique]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/roadm.png}
\end{center}
```

## Contexte

Savez-vous que notre filière possède un réseau national ? Nous avons acquis 6 nœuds de l’ancien réseau national de Netplus et l’avons redéployé dans nos labos. Nous avons amélioré la redondance de la plateforme en protégeant tous les liens de manière automatique : l’émetteur envoie le signal optique sur deux chemins différents à destination du nœud de réception. Un switch optique positionné devant le nœud de réception laisse passer le signal primaire tout en bloquant le secondaire. En cas de perte du signal primaire il switche automatiquement sur le signal de backup. Cette fonctionnalité est intéressante car la rapidité du switching de l’ordre de la ms est imbattable. Les deux chemins sont cependant utilisés en permanence ce qui divise la capacité du réseau par deux.
 

## Objectifs

Une amélioration intelligente a été développée qui n’utilise qu’un seul chemin. Dans le cas d’une perte du canal, l’émetteur est reconfiguré pour changer de longueur d’onde et emprunter le chemin de backup. Cette méthode a l’avantage de ne pas bloquer le lien de backup, mais le changement de couleur (ou longueur d’onde) n’est pas instantané. Durant ce travail de semestre le temps de reconfiguration du lien doit être analysé et jugé s’il est compatible pour différents services : Video ? Transfert de Fichier ou autre.

La fonctionnalité de changement de couleur induisant un changement de direction peut permettre non seulement de rediriger un canal vers un lien de protection mais aussi vers les noeuds dont le trafic a fortement augmenté. Ceci correspond au thème principal du travail de semestre. Il faut reprendre la fonctionnalité destinée à la protection et la coupler à la mesure de trafic sur un lien. Si le trafic dépasse une certaine limite, l’émetteur du lien flexible prend automatiquement la couleur qui l’envoie vers la destination surchargée. Le réseau ainsi réalisé devient intelligent et s’adapte automatiquement à la quantité de trafic. L’intelligence du réseau développé durant ce travail de semestre est particulièrement intéressante pour gérer des liaisons entre datacenters. En effet des liens peuvent être créés/supprimés en fonction des backups en phase de réalisation.
 

## Contraintes

Utiliser la plateforme « Ciena » du réseau backbone de l’école.
