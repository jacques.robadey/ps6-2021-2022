---
version: 2
titre: l'IoT au secours d'une mobilité douce saine et agréable

type de projet: Projet de semestre 6
année scolaire: 2021/2022
mandants:
  - service de la mobilité de la ville de Fribourg
  - Office Fédéral des ROUtes
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean-Roland Schuler
mots-clé: [Mobilité douce, LoRa, Internet des objets, trafic routier, particules fines, bruit routier, effort physique en environnement sain]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth]{img/grey_slow_mobility.jpg}
\end{center}
```

## Contexte

La commission fédérale de la mobilité vient d’accepter un projet d’étude de la mobilité douce en ville de Fribourg. La thématique est de s’assurer qu’elle se développe dans des conditions saines et agréables.
Pour ce l’école d’ingénieur et la ville de Fribourg proposent d’utiliser l’internet des objets pour mesurer les paramètres clés de cette mobilité. Les entreprises Decentlab et iav ont proposé leur collaboration pour les mesures de particules fines et de bruit. L’entreprise Franic SA propose de nous fournir des balises gps transportables sur tout type de véhicule.
Le projet de semestre consiste à l’utilisation de ces trois capteurs de manière mobile: sur des vélos ou des trottinettes. Les pistes cyclables actuelles souvent juxtaposées aux voies de très gros trafic pourraient être comparées à des tracés alternatifs. Les mesures devront aussi être nomadiques : les capteurs restent aux mêmes endroits durant une semaine pour illustrer les variations horaires et journalières. L’analyse des résultats est à faire par l’étudiant. Des propositions de tracés différents en fonction de l’horaire pourraient être un output, mais elles doivent être basées sur des mesures réelles.
Le travail de semestre a donc trois facettes : la réalisation de mesures fiables, le stockage et la visualisation de ces mesures et l’analyse critique des résultats
 

## Objectifs

Définir une campagne de mesure mobile et nomadique sur les axes routier et les chemins alternatifs de la ville de Fribourg et la réaliser en utilisant l’internet des objets.
Analyser les résultats avec les responsables de la mobilité de Fribourg et de l’office fédéral des routes.
Présenter les résultats de manière concrète en illustrant les dépendances géographiques et temporelles.


## Contraintes

Utiliser les capteurs de microparticules et de bruit et les balises gps des entreprises Decentlab, iav et respectivement Franic SA
