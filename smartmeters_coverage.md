---
version: 2
titre: Etude de la couverture réseau des compteurs Smart chez Groupe E SA

type de projet: Projet de semestre 6
année scolaire: 2021/2022
mandants:
  - groupe E
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Ioana Preda
mots-clé: [smartmeters, taux de couverture, réseau mobile, IoT, données géographique]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/smart_met.jpg}
\end{center}
```

## Contexte

Groupe E assure le rôle de gestionnaire de réseau de distribution (GRD) sur une zone de desserte qui couvre les cantons de Fribourg, Neuchâtel, et une partie des cantons de Vaud et de Berne, pour un total de ~250 000 points de mesure.
Groupe E a lancé en 2020 son projet de smart metering avec pour objectif principal d’équiper l’ensemble du parc de comptage de compteurs intelligents d’ici fin 2027. 
Lors d’un POC (proof of concept) en 2020, Groupe E a déployé environ 1000 compteurs intelligents de manière aléatoire. L’un de objectifs était, entre-autre, d’étudier la couverture réseau des compteurs. Ce taux étant proche de 95%, il a été établi que le moyen de communication est adéquat. Cependant, il est important de mieux comprendre les raisons pour lesquelles certains appareils ne communiquent pas. 
 

## Objectifs

A travers le projet proposé, l’idée est d’étudier les compteurs pour lesquels la communication ne passe pas afin de déterminer les paramètres principaux qui pourraient expliquer une mauvaise communication. En fonction des mesures terrain, des données terrain (ex. zone géographique, étage où se trouve le compteur, autre) et/ou d’autres paramètres, il serait demandé de pouvoir estimer la probabilité de couverture du réseau en connaissant en entrée les paramètres établis.  Ceci permettrait à Groupe E d’estimer les types d’endroits qui nécessiterait une solution pour améliorer la communication (ex. pose d’une antenne) potentiellement avant de se rendre sur place. 

## Contraintes

Etudier les pertes de connections spécifiques aux smartmeters choisis par le Groupe E
